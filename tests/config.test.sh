#!/bin/sh

ret=$(docker run --rm --name $test_container_name $test_image git version | grep "git version")

if [ "$?" != "0" ] || [ -z "$ret" ] ; then
  log 3 "testing image not successfull!"
  exit 1
fi

log 7 "detected git version: $ret"
